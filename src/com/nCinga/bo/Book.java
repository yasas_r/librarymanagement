package com.nCinga.bo;

import com.nCinga.bo.enums.Subject;

import java.util.List;
import java.util.Objects;

public class Book {

    private final String name;
    private final Subject subject;

    public Book(String name, Subject subject) {
        this.name = name;
        this.subject = subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getName().equals(book.getName()) &&
                getSubject() == book.getSubject();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSubject());
    }

    public String getName() {
        return name;
    }

    public Subject getSubject() {
        return subject;
    }
}
