package com.nCinga.bo;

import com.nCinga.bo.enums.Degree;
import java.util.Objects;

public class Student {

    private final int rollNo;
    private final String name;
    private final String batch;
    private final char section;
    private final Degree degree;
    private int issuedCount=0;

    public static final int maxIssuedBookCount_B_TECH = 5;
    public static final int maxIssuedBookCount_M_TECH = 10;

    public Student(int rollNo, String name, String batch, char section, Degree degree) {
        this.rollNo = rollNo;
        this.name = name;
        this.batch = batch;
        this.section = section;
        this.degree = degree;
    }

    public int getRollNo() {
        return rollNo;
    }

    public String getName() {
        return name;
    }

    public int getMaxIssuedBookCount() {
        if (this.getDegree() == Degree.B_TECH)
            return maxIssuedBookCount_B_TECH;
        else if (this.getDegree() == Degree.M_TECH)
            return maxIssuedBookCount_M_TECH;
        return 0;
    }

    public String getBatch() {
        return batch;
    }

    public char getSection() {
        return section;
    }

    public Degree getDegree() {
        return degree;
    }

    public int getIssuedCount() {
        return this.issuedCount;
    }

    public void increaseIssuedCount(){
        this.issuedCount++;
    }
    public void decreaseIssuedCount(){
        this.issuedCount--;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getRollNo() == student.getRollNo();
    }

}
