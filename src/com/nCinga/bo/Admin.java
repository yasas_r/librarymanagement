package com.nCinga.bo;

import java.util.Objects;

public class Admin {

    private final int id;
    private final String name;

    public Admin(int id, String name1) {
        this.id = id;
        this.name = name1;

    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Admin)) return false;
        Admin admin = (Admin) o;
        return getId() == admin.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }
}
