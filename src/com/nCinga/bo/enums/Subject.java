package com.nCinga.bo.enums;

public enum Subject {
    MATHS,
    PHYSICS,
    CHEMISTRY,
    BIO
}
