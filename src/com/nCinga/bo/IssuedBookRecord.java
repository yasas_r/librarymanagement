package com.nCinga.bo;

import java.util.Objects;

public class IssuedBookRecord {

    private int id = 0;
    private static int autoIncId = 0;
    private final Student student;
    private final Book book;
    private final Admin admin;

    public IssuedBookRecord(Student student, Book book, Admin admin) {
        this.student = student;
        this.book = book;
        this.admin = admin;
        this.id = autoIncrementId();
    }

    public int getId() {
        return id;
    }

    public Student getStudent() {
        return student;
    }

    public Book getBook() {
        return book;
    }

    public Admin getAdmin() {
        return admin;
    }

    public int autoIncrementId() {

        return ++autoIncId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssuedBookRecord)) return false;
        IssuedBookRecord that = (IssuedBookRecord) o;
        return Objects.equals(getStudent(), that.getStudent()) &&
                Objects.equals(getBook(), that.getBook()) &&
                Objects.equals(getAdmin(), that.getAdmin());
    }

}
