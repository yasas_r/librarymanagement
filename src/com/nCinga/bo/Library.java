package com.nCinga.bo;

import com.nCinga.bo.enums.Degree;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Library {

    private final String name;
    private final int number;

    public Library(int number,String name) {
        this.name = name;
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Library)) return false;
        Library library = (Library) o;
        return getNumber() == library.getNumber() &&
                getName().equals(library.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getNumber());
    }
}
