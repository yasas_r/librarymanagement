package com.nCinga.bo;

import java.util.Objects;

public class LibraryBookRecord {

    private final Library library;
    private final Book book;
    private final int totalCount;
    private int issuedBookCount;

    public LibraryBookRecord(Library library, Book book, int totalCount) {
        this.library = library;
        this.book = book;
        this.totalCount = totalCount;
        this.issuedBookCount = 0;
    }

    public LibraryBookRecord(Library library, Book book, int totalCount, int issuedBookCount) {
        this.library = library;
        this.book = book;
        this.totalCount = totalCount;
        this.issuedBookCount = issuedBookCount;
    }

    public int getIssuedBookCount() {
        return issuedBookCount;
    }

    public Library getLibrary() {
        return library;
    }

    public Book getBook() {
        return book;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void increaseIssuedBookCountByOne() {
        this.issuedBookCount++;
    }

    public void decreaseIssuedBookCountByOne() {
        this.issuedBookCount--;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LibraryBookRecord)) return false;
        LibraryBookRecord that = (LibraryBookRecord) o;
        return getTotalCount() == that.getTotalCount() &&
                getIssuedBookCount() == that.getIssuedBookCount() &&
                getLibrary().equals(that.getLibrary()) &&
                getBook().equals(that.getBook());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLibrary(), getBook(), getTotalCount(), getIssuedBookCount());
    }
}
