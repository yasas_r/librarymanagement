package com.nCinga;

import com.nCinga.bo.*;
import com.nCinga.bo.enums.Degree;
import com.nCinga.bo.enums.Subject;
import com.nCinga.dao.*;
import com.nCinga.service.impl.LibraryRepositoryServiceImpl;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        //------ Adding details

        AdminDao adminDao = AdminDao.getInstance();
        adminDao.addAdmin(new Admin(101, "admin name1"));
        adminDao.addAdmin(new Admin(102, "admin name2"));
        adminDao.addAdmin(new Admin(103, "admin name3"));

        LibraryDao libraryDao = LibraryDao.getInstance();
        libraryDao.addLibrary(new Library(001, "Colombo Library"));
        libraryDao.addLibrary(new Library(002, "Japura Library"));
        libraryDao.addLibrary(new Library(003, "Moratuva Library"));

        StudentsDao studentsDao = StudentsDao.getInstance();
        studentsDao.addStudent(new Student(111, "yasas", "15", 'A', Degree.B_TECH));
        studentsDao.addStudent(new Student(112, "salitha", "16", 'B', Degree.M_TECH));
        studentsDao.addStudent(new Student(113, "charitha", "16", 'C', Degree.B_TECH));

        LibraryBookRecordDao libraryBookRecordDao = LibraryBookRecordDao.getInstance();
        libraryBookRecordDao.addLibraryBookRecord(new LibraryBookRecord(new Library(001, "Colombo Library"),
                new Book("Book2", Subject.MATHS),20));
        libraryBookRecordDao.addLibraryBookRecord(new LibraryBookRecord(new Library(001, "Colombo Library"),
                new Book("Book3", Subject.MATHS),30));
        libraryBookRecordDao.addLibraryBookRecord(new LibraryBookRecord(new Library(002, "Japura Library"),
                new Book("Book3", Subject.MATHS),30));

        IssueBookRecordDao issueBookRecordDao = IssueBookRecordDao.getInstance();
        issueBookRecordDao.addIssueBookRecord(new IssuedBookRecord(
                new Student(111, "yasas", "15", 'A', Degree.B_TECH),
                new Book("Book2", Subject.MATHS),
                new Admin(102, "admin name2")));
        issueBookRecordDao.addIssueBookRecord(new IssuedBookRecord(
                new Student(112, "salitha", "16", 'B', Degree.M_TECH),
                new Book("Book1", Subject.BIO),
                new Admin(102, "admin name1")));
        issueBookRecordDao.addIssueBookRecord(new IssuedBookRecord(
                new Student(113, "charitha", "16", 'C', Degree.B_TECH),
                new Book("Book3", Subject.MATHS),
                new Admin(102, "admin name1")));


        //----------- Issue Book

        LibraryRepositoryServiceImpl libraryRepositoryService = new LibraryRepositoryServiceImpl();

        Student student = new Student(111, "yasas", "15", 'A', Degree.B_TECH);
        Book book = new Book("Book2", Subject.MATHS);
        Admin admin = new Admin(102, "admin name2");
        Library library = new Library(001, "Colombo Library");

        libraryRepositoryService.issueBookService(student, book, admin, library);

        libraryRepositoryService.returnBookService(student, book, admin, library);


    }
}
