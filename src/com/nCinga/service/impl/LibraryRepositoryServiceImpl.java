package com.nCinga.service.impl;

import com.nCinga.bo.*;
import com.nCinga.dao.*;
import com.nCinga.exceptions.CantIssueLibraryBook;
import com.nCinga.exceptions.CantReturnLibraryBook;
import com.nCinga.service.LibraryRepositoryService;

public class LibraryRepositoryServiceImpl implements LibraryRepositoryService {

    AdminDao adminDao;
    LibraryDao libraryDao;
    StudentsDao studentsDao;
    LibraryBookRecordDao libraryBookRecordDao;
    IssueBookRecordDao issueBookRecordDao;


    public LibraryRepositoryServiceImpl() {
        adminDao = AdminDao.getInstance();
        libraryDao = LibraryDao.getInstance();
        studentsDao = StudentsDao.getInstance();
        libraryBookRecordDao = LibraryBookRecordDao.getInstance();
        issueBookRecordDao = IssueBookRecordDao.getInstance();
    }

    private void issueBookFromLibrary(Student student, Book book, Admin admin, Library library) {
        issueBookRecordDao.addIssueBookRecord(new IssuedBookRecord(student, book, admin));
        libraryBookRecordDao.findLibraryBookRecord(library, book).increaseIssuedBookCountByOne();
        studentsDao.increaseIssuedCount(student);
        System.out.println(book.getName()+" is issued to "+student.getName()+" from "+library.getName()+" by "+ admin.getName());
    }

    private boolean validateBookThatIsGoingToIssue(Student student, Book book, Admin admin, Library library) {

        return libraryDao.isValidLibrary(library) &&
                libraryBookRecordDao.isBookExists(library, book) &&
                libraryBookRecordDao.isBookAvailable(library, book) &&
                adminDao.isValidAdmin(admin) && studentsDao.isValidStudent(student) &&
                issueBookRecordDao.canIssueBookToStudent(student);
    }

    public void issueBookService(Student student, Book book, Admin admin, Library library) {

        if (validateBookThatIsGoingToIssue(student, book, admin, library))
            issueBookFromLibrary(student, book, admin, library);
        else
            throw new CantIssueLibraryBook("The book cant be issued");
    }

    private void returnBookFromLibrary(Student student, Book book, Admin admin, Library library) {
        studentsDao.decreaseIssuedCount(student);
        System.out.println(book.getName()+" is returned to "+library.getName()+" by "+student.getName());
    }

    private boolean validateBookThatIsGoingToReturn(Student student, Book book, Admin admin, Library library) {

        return libraryDao.isValidLibrary(library) &&
                libraryBookRecordDao.isBookExists(library, book) &&
                adminDao.isValidAdmin(admin) && studentsDao.isValidStudent(student);
    }

    public void returnBookService(Student student, Book book, Admin admin, Library library) {

        if (validateBookThatIsGoingToReturn(student, book, admin, library))
            returnBookFromLibrary(student, book, admin, library);
        else
            throw new CantReturnLibraryBook("The book cant be returned");
    }

}
