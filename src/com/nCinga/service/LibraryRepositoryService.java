package com.nCinga.service;

import com.nCinga.bo.Admin;
import com.nCinga.bo.Book;
import com.nCinga.bo.Library;
import com.nCinga.bo.Student;

public interface LibraryRepositoryService {

    void issueBookService(Student student, Book book, Admin admin, Library library);
    void returnBookService(Student student, Book book, Admin admin, Library library);
}
