package com.nCinga.exceptions;

public class CantReturnLibraryBook extends RuntimeException {

    public CantReturnLibraryBook(String message){
        super(message);
    }
}
