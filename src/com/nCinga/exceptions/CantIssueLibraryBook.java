package com.nCinga.exceptions;

public class CantIssueLibraryBook extends RuntimeException{

    public CantIssueLibraryBook(String message){
        super(message);
    }
}

