package com.nCinga.dao;

import com.nCinga.bo.Admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AdminDao {

    private static AdminDao adminDao;

    private List<Admin> admins;

    private AdminDao() {
        admins = new ArrayList<Admin>();
    }

    public static AdminDao getInstance() {
        adminDao = adminDao == null ? new AdminDao() : adminDao;
        return adminDao;
    }

    public void addAdmin(Admin admin) {
        admins.add(admin);
    }

    public void removeAdmin(Admin admin) {
        admins.remove(admin);
    }


    public Admin findAdmin(int id) {
        Admin admin = null;
        for (int i = 0; i < admins.size(); i++) {
            admin = (Admin) admins.get(i);
            if (admin.getId() == id) {
                return admin;
            }
        }
        return admin;
    }

    public void updateAdmin(Admin oldAdmin, Admin newAdmin) {
        int index = admins.indexOf(oldAdmin);
        if (index != -1)
            admins.set(index, newAdmin);
    }

    public void updateAdminName(int id, String name) {
        Admin admin = findAdmin(id);
        if (admin != null) {
            int index = admins.indexOf(admin);
            if (index != -1) {
                admins.set(index, new Admin(id, name));
            }
        }
    }

    public boolean isValidAdmin(Admin admin) {
        int id = admin.getId();
        if (findAdmin(id) != null)
            return true;
        else
            return false;
    }

}