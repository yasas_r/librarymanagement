package com.nCinga.dao;


import com.nCinga.bo.*;
import com.nCinga.bo.enums.Degree;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IssueBookRecordDao {

    private static IssueBookRecordDao issueBookRecordDao;

    private List<IssuedBookRecord> issuedBookRecords;

    private IssueBookRecordDao() {
        issuedBookRecords = new ArrayList<IssuedBookRecord>();
    }

    public static IssueBookRecordDao getInstance() {
        issueBookRecordDao = issueBookRecordDao == null ? new IssueBookRecordDao() : issueBookRecordDao;
        return issueBookRecordDao;
    }

    public void addIssueBookRecord(IssuedBookRecord issuedBookRecord) {
        issuedBookRecords.add(issuedBookRecord);
    }

    public void removeIssueBookRecord(IssuedBookRecord issuedBookRecord) {
        issuedBookRecords.remove(issuedBookRecord);
    }

    public IssuedBookRecord findIssueBookRecord(int id) {
        IssuedBookRecord issuedBookRecord = null;
        for (int i = 0; i < issuedBookRecords.size(); i++) {
            issuedBookRecord = issuedBookRecords.get(i);
            if (issuedBookRecord.getId() == id) {
                return issuedBookRecord;
            }
        }
        return issuedBookRecord;
    }


    public void updateIssuedBookRecord(IssuedBookRecord oldIssuedBookRecord, IssuedBookRecord newIssuedBookRecord) {
        int index = issuedBookRecords.indexOf(oldIssuedBookRecord);
        issuedBookRecords.set(index, newIssuedBookRecord);

    }

    public void updateIssuedBookRecordById(int id, Student student, Book book, Admin admin) {
        IssuedBookRecord issuedBookRecord = findIssueBookRecord(id);
        if (issuedBookRecord != null) {
            int index = issuedBookRecords.indexOf(issuedBookRecord);
            if (index != -1) {
                issuedBookRecords.set(index, new IssuedBookRecord(student, book, admin));
            }
        }
    }

    public int issuedBookCountStudent(Student student) {
        int issuedCount = 0;
        for (int i = 0; i < issuedBookRecords.size(); i++) {
            if (issuedBookRecords.get(i).getStudent().getRollNo() == student.getRollNo())
                issuedCount++;
        }
        return issuedCount;
    }

    public boolean isRightIssuedRecord(IssuedBookRecord issuedBookRecord) {
        if (issuedBookRecords.equals(issuedBookRecord))
            return true;
        else
            return false;

    }

    public boolean canIssueBookToStudent(Student student) {
        if (student.getDegree() == Degree.B_TECH && student.getIssuedCount() < Student.maxIssuedBookCount_B_TECH)
            return true;
        else if (student.getDegree() == Degree.M_TECH && student.getIssuedCount() < Student.maxIssuedBookCount_M_TECH)
            return true;
        else
            return false;
    }

}
