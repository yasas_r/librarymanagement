package com.nCinga.dao;

import com.nCinga.bo.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LibraryDao {

    private static LibraryDao libraryDao;

    private List<Library> libraries;

    private LibraryDao() {
        libraries = new ArrayList<Library>();
    }

    public static LibraryDao getInstance() {
        libraryDao = libraryDao == null ? new LibraryDao() : libraryDao;
        return libraryDao;
    }

    public void addLibrary(Library library) {
        libraries.add(library);
    }

    public void removeLibrary(Library library) {
        libraries.remove(library);
    }

    public Library findLibrary(int number) {
        Library library = null;
        for (int i = 0; i < libraries.size(); i++) {
            library = (Library) libraries.get(i);
            if (library.getNumber() == number) {
                return library;
            }
        }
        return library;
    }

    public void updateLibrary(Library oldLibrary, Library newLibrary) {
        int index = libraries.indexOf(oldLibrary);
        libraries.set(index, newLibrary);

    }

    public boolean isValidLibrary(Library library) {

        int number = library.getNumber();

        if (findLibrary(number) != null)
            return true;
        else
            return false;

    }


}
