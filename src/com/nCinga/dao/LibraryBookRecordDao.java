package com.nCinga.dao;

import com.nCinga.bo.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LibraryBookRecordDao {

    private static LibraryBookRecordDao libraryBookRecordDao;

    private List<LibraryBookRecord> libraryBookRecords = new ArrayList<LibraryBookRecord>();

    private LibraryBookRecordDao() {
        libraryBookRecords = new ArrayList<LibraryBookRecord>();
    }

    public static LibraryBookRecordDao getInstance() {
        libraryBookRecordDao = libraryBookRecordDao == null ? new LibraryBookRecordDao() : libraryBookRecordDao;
        return libraryBookRecordDao;
    }

    public void addLibraryBookRecord(LibraryBookRecord libraryBookRecord) {
        libraryBookRecords.add(libraryBookRecord);
    }

    public LibraryBookRecord findLibraryBookRecord(Library library, Book book) {

        if (libraryBookRecords.size() != 0) {
            LibraryBookRecord libraryBookRecord = null;
            int sizeOfLibraryBookRecords = libraryBookRecords.size();
            for (int i = 0; i < sizeOfLibraryBookRecords; i++) {
                libraryBookRecord = libraryBookRecords.get(i);
                if (libraryBookRecord.getLibrary().getNumber() == library.getNumber() &&
                        libraryBookRecord.getBook().getName() == book.getName() &&
                        libraryBookRecord.getBook().getSubject() == book.getSubject()) {
                    return libraryBookRecord;
                }
            }
        } else
            return null;
        return null;
    }

    public boolean isBookExists(Library library, Book book) {

        if (libraryBookRecords.size() != 0) {
            LibraryBookRecord libraryBookRecord = findLibraryBookRecord(library, book);
            if (libraryBookRecord != null)
                return true;
        } else
            return false;
        return false;
    }

    public boolean isBookAvailable(Library library, Book book) {

        if (libraryBookRecords.size() != 0 && isBookExists(library, book)) {
            LibraryBookRecord libraryBookRecord = findLibraryBookRecord(library, book);
            if (libraryBookRecord != null) {
                int availableCount = libraryBookRecord.getTotalCount() - libraryBookRecord.getIssuedBookCount();
                if (availableCount != 0)
                    return true;
            }
        } else
            return false;
        return false;
    }


}
