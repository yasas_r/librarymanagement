package com.nCinga.dao;

import com.nCinga.bo.Admin;
import com.nCinga.bo.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StudentsDao {

    private static StudentsDao studentsDao;

    private List<Student> students;

    private StudentsDao() {
        students = new ArrayList<Student>();
    }

    public static StudentsDao getInstance() {
        studentsDao = studentsDao == null ? new StudentsDao() : studentsDao;
        return studentsDao;
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void removeStudent(Student student) {
        students.remove(student);
    }

    public Student findStudent(int rollNo) {
        Student student = null;
        for (int i = 0; i < students.size(); i++) {
            student = students.get(i);
            if (student.getRollNo() == rollNo) {
                return student;
            }
        }
        return student;
    }

    public void updateStudent(Student oldStudent, Student newStudent) {
        int index = students.indexOf(oldStudent);
        if(index!=-1)
            students.set(index, newStudent);
    }

    public void updateStudent() {

    }

    public boolean isValidStudent(Student student) {
        int rollNo = student.getRollNo();
        if (findStudent(rollNo) != null)
            return true;
        else
            return false;
    }

    public void increaseIssuedCount(Student student){
        findStudent(student.getRollNo()).increaseIssuedCount();
    }
    public void decreaseIssuedCount(Student student){
        findStudent(student.getRollNo()).decreaseIssuedCount();
    }

}
